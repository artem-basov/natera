package ru.basov.interview.natera;

import java.util.Objects;

public final class Edge<V> {
    private final V predecessor;
    private final V successor;
    private final boolean isDirected;

    public Edge(V predecessor, V successor, boolean isDirected) {
        this.predecessor = predecessor;
        this.successor = successor;
        this.isDirected = isDirected;
    }

    public V predecessor() {
        return predecessor;
    }

    public V successor() {
        return successor;
    }

    @Override
    public String toString() {
        return predecessor.toString() + "---" + (isDirected ? ">" : "") + successor.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge<?> edge = (Edge<?>) o;
        return isDirected == edge.isDirected &&
                Objects.equals(predecessor, edge.predecessor) &&
                Objects.equals(successor, edge.successor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(predecessor, successor, isDirected);
    }
}
