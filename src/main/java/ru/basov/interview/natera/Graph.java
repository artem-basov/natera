package ru.basov.interview.natera;

import java.util.List;
import java.util.Set;

public interface Graph<V> {

    /**
     * Adds vertex to this graph
     *
     * @param v vertex to be added
     * @return true if vertex was added
     */
    public boolean addVertex(V v);

    /**
     * Adds edge to the graph
     *
     * @return
     */
    public boolean addEdge(V a, V b);

    public List<Edge<V>> findPath(V from, V to);

    public boolean isDirected();

    public boolean allowsSelfLoops();

    Set<V> vertices();

    Set<V> adjacentTo(V node);
}
