package ru.basov.interview.natera;

import java.util.Set;

public interface VertexEdges<V> {

    Set<V> successors();

    Set<V> adjacentNodes();

    void addSuccessor(V vertex);

    void addPredecessor(V vertex);
}
