package ru.basov.interview.natera;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class UndirectedVertexEdges<V> implements VertexEdges<V> {
    private final Set<V> adjacentVertexes = new HashSet<>();

    @Override
    public Set<V> successors() {
        return adjacentNodes();
    }

    @Override
    public Set<V> adjacentNodes() {
        return Collections.unmodifiableSet(adjacentVertexes);
    }

    @Override
    public void addSuccessor(V vertex) {
        adjacentVertexes.add(vertex);
    }

    @Override
    public void addPredecessor(V vertex) {
        addSuccessor(vertex);
    }
}
