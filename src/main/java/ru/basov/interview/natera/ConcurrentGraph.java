package ru.basov.interview.natera;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class ConcurrentGraph<V> implements Graph<V> {
    private final boolean isDirected;
    private final boolean allowSelfLoops;
    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final HashMap<V, VertexEdges<V>> edges = new HashMap<>();

    public ConcurrentGraph(boolean isDirected, boolean allowSelfLoops) {
        this.isDirected = isDirected;
        this.allowSelfLoops = allowSelfLoops;
    }

    @Override
    public boolean addVertex(V v) {
        rwLock.writeLock().lock();
        try {
            return edges.putIfAbsent(v, newEdges()) == null;
        } finally {
            rwLock.writeLock().unlock();
        }
    }

    private VertexEdges<V> newEdges() {
        return isDirected() ? new DirectedVertexEdges<>()
                : new UndirectedVertexEdges<>();
    }

    @Override
    public boolean addEdge(V a, V b) {
        if (!allowSelfLoops && a.equals(b)) {
            throw new IllegalArgumentException("Cannot add self-looped edge. Construct ConcurrentGraph with allowSelfLoops=true");
        }
        rwLock.writeLock().lock();
        try {
            edges.computeIfAbsent(a, e -> newEdges()).addSuccessor(b);
            edges.computeIfAbsent(b, e -> newEdges()).addPredecessor(a);
            return true;
        } finally {
            rwLock.writeLock().unlock();
        }
    }

    @Override
    public List<Edge<V>> findPath(V from, V to) {
        rwLock.readLock().lock();
        try {
            return aStar(from, to);
        } finally {
            rwLock.readLock().unlock();
        }
    }

    @Override
    public boolean isDirected() {
        return isDirected;
    }

    @Override
    public boolean allowsSelfLoops() {
        return allowSelfLoops;
    }

    @Override
    public Set<V> vertices() {
        rwLock.readLock().lock();
        try {
            return Collections.unmodifiableSet(edges.keySet());
        } finally {
            rwLock.readLock().unlock();
        }
    }

    @Override
    public Set<V> adjacentTo(V node) {
        rwLock.readLock().lock();
        try {
            return Collections.unmodifiableSet(edges.get(node).adjacentNodes());
        } finally {
            rwLock.readLock().unlock();
        }
    }

    private List<Edge<V>> aStar(V from, V to) {
        HashSet<V> closedSet = new HashSet<>();
        ArrayList<V> openSet = new ArrayList<>();
        openSet.add(from);
        HashMap<V, V> cameFrom = new HashMap<>();
        HashMap<V, Integer> gScore = new HashMap<>();
        gScore.put(from, 0);

        // Total cost from start to target
        HashMap<V, Integer> fScore = new HashMap<>();
        for (V vertex : vertices()) {
            fScore.put(vertex, Integer.MAX_VALUE);
        }
        fScore.put(from, 1); // 1 point for each step

        Comparator<V> totalCostComparator = Comparator.comparingInt(fScore::get);

        while (!openSet.isEmpty()) {
            V current = openSet.get(0);
            if (current.equals(to)) {
                return reconstructPath(cameFrom, to);
            }
            openSet.remove(0);
            closedSet.add(current);

            Set<V> successors = this.edges.get(current).successors();
            for (V successor : successors) {
                if (closedSet.contains(successor)) {
                    continue;
                }

                int tentativeScore = gScore.get(current) + 1; //until weights introduced we just increment by 1

                if (!openSet.contains(successor)) {
                    openSet.add(successor);
                } else if (tentativeScore >= gScore.get(successor)) {
                    continue;
                }

                // best path so far
                cameFrom.put(successor, current);
                gScore.put(successor, tentativeScore);
                int estScore = gScore.get(successor) + 1;  // 1 point for each step
                fScore.put(successor, estScore);

                openSet.sort(totalCostComparator);
            }
        }
        return Collections.emptyList();
    }

    private List<Edge<V>> reconstructPath(HashMap<V, V> backtrack, V to) {
        ArrayList<Edge<V>> path = new ArrayList<>();
        V current = to;
        while (current != null) {
            V previous = current;
            current = backtrack.get(current);
            if (current != null) {
                path.add(new Edge<>(current, previous, isDirected));
            }
        }
        Collections.reverse(path);
        return path;
    }
}
