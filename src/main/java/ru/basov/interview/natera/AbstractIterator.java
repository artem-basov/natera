package ru.basov.interview.natera;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class AbstractIterator<T> implements Iterator<T> {
    private T next = getNext();

    protected abstract T getNext();

    @Override
    public boolean hasNext() {
        return next != null;
    }

    @Override
    public T next() {
        if (!hasNext()) throw new NoSuchElementException();
        T current = next;
        next = getNext();
        return current;
    }
}
