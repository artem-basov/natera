package ru.basov.interview.natera;

import java.util.AbstractSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Not thread safe!
 *
 * @param <V> vertex type
 */
public final class DirectedVertexEdges<V> implements VertexEdges<V> {
    private static final Object PREDECESSOR = new Object();
    private static final Object SUCCESSOR = new Object();

    private final Map<V, Object> adjacentVertexes = new HashMap<>();
    private int successorCount;

    @Override
    public Set<V> successors() {
        return new AbstractSet<V>() {
            @Override
            public Iterator<V> iterator() {
                Iterator<Map.Entry<V, Object>> itr = adjacentVertexes.entrySet().iterator();
                return new AbstractIterator<V>() {
                    @Override
                    public V getNext() {
                        while (itr.hasNext()) {
                            Map.Entry<V, Object> entry = itr.next();
                            if (isSuccessor(entry.getValue())) {
                                return entry.getKey();
                            }
                        }
                        return null;
                    }
                };
            }

            @Override
            public int size() {
                return successorCount;
            }
        };
    }

    private boolean isSuccessor(Object value) {
        return value == SUCCESSOR;
    }

    @Override
    public Set<V> adjacentNodes() {
        return Collections.unmodifiableSet(adjacentVertexes.keySet());
    }

    @Override
    public void addSuccessor(V vertex) {
        adjacentVertexes.put(vertex, SUCCESSOR);
        successorCount++;
    }

    @Override
    public void addPredecessor(V vertex) {
        adjacentVertexes.put(vertex, PREDECESSOR);
    }
}
