package ru.basov.interview.natera;

import org.assertj.core.api.Fail;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class DirectedGraph {
    @Test(expected = IllegalArgumentException.class)
    public void does_not_allow_self_loops_if_not_constructed_properly() {
        // given
        ConcurrentGraph<String> prohibitingSelfLoopsGraph = new ConcurrentGraph<>(true, false);
        prohibitingSelfLoopsGraph.addVertex("A");
        // when
        prohibitingSelfLoopsGraph.addEdge("A", "A");
        // then
        Fail.shouldHaveThrown(IllegalArgumentException.class);
    }

    @Test
    public void returns_empty_path_if_no_path_were_found() {
        // given
        ConcurrentGraph<String> graph = new ConcurrentGraph<>(true, false);
        graph.addVertex("A");
        graph.addVertex("B");
        // when
        List<Edge<String>> path = graph.findPath("A", "B");
        // then
        assertThat(path).isEmpty();
    }

    @Test
    public void finds_path_respecting_edges_directions() {
        // given
        ConcurrentGraph<String> graph = new ConcurrentGraph<>(true, false);
        graph.addVertex("A");
        graph.addVertex("B");
        graph.addVertex("BA");
        graph.addVertex("D");
        graph.addVertex("AA");
        graph.addVertex("AAA");
        graph.addVertex("AB");

        graph.addEdge("A", "B");
        graph.addEdge("A", "D");
        graph.addEdge("A", "AA");
        graph.addEdge("AAA", "AA");
        graph.addEdge("AAA", "A"); // not this edge. If not direction it would have been a shortest one.
        graph.addEdge("B", "BA");
        graph.addEdge("BA", "AAA");
        graph.addEdge("D", "BA");
        // when
        List<Edge<String>> path = graph.findPath("A", "AAA");
        // then
        assertThat(path).isNotEmpty()
                .containsExactly(
                        new Edge<>("A", "B", true),
                        new Edge<>("B", "BA", true),
                        new Edge<>("BA", "AAA", true));
    }

    @Test
    public void finds_path_ignoring_cycles() {
        // given
        ConcurrentGraph<String> graph = new ConcurrentGraph<String>(true, true);
        graph.addVertex("A");
        graph.addVertex("B");
        graph.addVertex("C");

        graph.addEdge("A", "B");
        graph.addEdge("B", "B");
        graph.addEdge("B", "C");

        // when
        List<Edge<String>> path = graph.findPath("A", "C");
        // then
        assertThat(path).isNotEmpty()
                .containsExactly(
                        new Edge<>("A", "B", true),
                        new Edge<>("B", "C", true));
    }
}