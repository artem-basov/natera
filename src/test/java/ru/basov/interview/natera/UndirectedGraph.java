package ru.basov.interview.natera;

import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


public class UndirectedGraph {
    @Test
    public void vertexes_are_not_adjacent_unless_edge_connects_them() {
        // given
        ConcurrentGraph<String> graph = new ConcurrentGraph<>(false, false);
        graph.addVertex("A");
        graph.addVertex("B");
        // when
        Set<String> aNeighbours = graph.adjacentTo("A");
        // then
        assertThat(aNeighbours).isEmpty();
        // and when
        graph.addEdge("A", "B");
        // then
        Set<String> aNeighboursConnected = graph.adjacentTo("A");
        assertThat(aNeighboursConnected)
                .isNotEmpty()
                .containsExactlyInAnyOrder("B");
    }

    @Test
    public void ignores_parameter_order_of_edge() {
        // given
        ConcurrentGraph<String> graph = new ConcurrentGraph<>(false, false);
        graph.addVertex("A");
        graph.addVertex("B");
        graph.addEdge("A", "B");
        // when
        List<Edge<String>> aToB = graph.findPath("A", "B");
        List<Edge<String>> bToA = graph.findPath("B", "A");
        // then
        assertThat(aToB)
                .isNotEmpty()
                .containsExactly(new Edge<>("A", "B", false));
        assertThat(bToA)
                .isNotEmpty()
                .containsExactly(new Edge<>("B", "A", false));
    }
}